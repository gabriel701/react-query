import { useQuery } from "@tanstack/react-query";
import { githubApi } from "../../api/githubApi";
import { Label } from "../../interfaces/labels";
import { sleepApp } from '../helpers/sleep';


const getLabels = async() : Promise<Label[]> => {
    await sleepApp(2);
    const { data } = await githubApi.get<Label[]>('/labels')
    return data;
  }
  
export const useLabels = () =>{
  const labelsQuery = useQuery (
    ['labels'],
    getLabels,{
        staleTime: 1000*60*60,

        placeholderData:  [
          {
            id: 204945357,
            node_id: "MDU6TGFiZWwyMDQ5NDUzNTc=",
            url: "https://api.github.com/repos/facebook/react/labels/Component:%20Shallow%20Renderer",
            name: "Component: Shallow Renderer",
            color: "eb6420",
            default: false,
          },{
            id: 588833528,
            node_id: "MDU6TGFiZWw1ODg4MzM1Mjg=",
            url: "https://api.github.com/repos/facebook/react/labels/Difficulty:%20medium",
            name: "Difficulty: medium",
            color: "fbca04",
            default: false,
          }
        ]
    }
  );

    return {
        labelsQuery
    };
}
